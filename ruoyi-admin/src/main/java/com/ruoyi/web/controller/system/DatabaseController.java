package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysDatabase;
import com.ruoyi.system.service.IDatabaseService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 数据源Controller
 * 
 * @author hy
 * @date 2021-01-04
 */
@RestController
@RequestMapping("/tenant/database")
public class DatabaseController extends BaseController
{
    @Autowired
    private IDatabaseService databaseService;

    /**
     * 查询数据源列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:database:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysDatabase database)
    {
        startPage();
        List<SysDatabase> list = databaseService.selectDatabaseList(database);
        return getDataTable(list);
    }
    /**
     * 查询数据源列表
     */
    @GetMapping("/listWithOutPage")
    @PreAuthorize("@ss.hasPermi('tenant:database:list')")
    public AjaxResult listWithOutPage()
    {
        List<SysDatabase> list = databaseService.selectDatabaseList(new SysDatabase());
        return AjaxResult.success(list);
    }
    /**
     * 导出数据源列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:database:export')")
    @Log(title = "数据源", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysDatabase database)
    {
        List<SysDatabase> list = databaseService.selectDatabaseList(database);
        ExcelUtil<SysDatabase> util = new ExcelUtil<SysDatabase>(SysDatabase.class);
        return util.exportExcel(list, "database");
    }

    /**
     * 获取数据源详细信息
     */
    @PreAuthorize("@ss.hasPermi('tenant:database:query')")
    @GetMapping(value = "/{databaseId}")
    public AjaxResult getInfo(@PathVariable("databaseId") Long databaseId)
    {
        return AjaxResult.success(databaseService.selectDatabaseById(databaseId));
    }

    /**
     * 新增数据源
     */
    @PreAuthorize("@ss.hasPermi('tenant:database:add')")
    @Log(title = "数据源", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysDatabase database)
    {
        return toAjax(databaseService.insertDatabase(database));
    }

    /**
     * 修改数据源
     */
    @PreAuthorize("@ss.hasPermi('tenant:database:edit')")
    @Log(title = "数据源", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysDatabase database)
    {
        return toAjax(databaseService.updateDatabase(database));
    }

    /**
     * 删除数据源
     */
    @PreAuthorize("@ss.hasPermi('tenant:database:remove')")
    @Log(title = "数据源", businessType = BusinessType.DELETE)
	@DeleteMapping("/{databaseIds}")
    public AjaxResult remove(@PathVariable Long[] databaseIds)
    {
        return toAjax(databaseService.deleteDatabaseByIds(databaseIds));
    }
}
