package com.ruoyi.system.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysAuthorization;
import com.ruoyi.system.service.ISysAuthorizationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 授权信息Controller
 * 
 * @author hy
 * @date 2021-01-06
 */
@RestController
@RequestMapping("/tenant/authorization")
public class SysAuthorizationController extends BaseController
{
    @Autowired
    private ISysAuthorizationService sysAuthorizationService;

    /**
     * 查询授权信息列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:authorization:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysAuthorization sysAuthorization)
    {
        startPage();
        List<SysAuthorization> list = sysAuthorizationService.selectSysAuthorizationList(sysAuthorization);
        return getDataTable(list);
    }

    /**
     * 导出授权信息列表
     */
    @PreAuthorize("@ss.hasPermi('tenant:authorization:export')")
    @Log(title = "授权信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysAuthorization sysAuthorization)
    {
        List<SysAuthorization> list = sysAuthorizationService.selectSysAuthorizationList(sysAuthorization);
        ExcelUtil<SysAuthorization> util = new ExcelUtil<SysAuthorization>(SysAuthorization.class);
        return util.exportExcel(list, "authorization");
    }

    /**
     * 获取授权信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('tenant:authorization:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(sysAuthorizationService.selectSysAuthorizationById(id));
    }

    /**
     * 新增授权信息
     */
    @PreAuthorize("@ss.hasPermi('tenant:authorization:add')")
    @Log(title = "授权信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysAuthorization sysAuthorization)
    {
        return toAjax(sysAuthorizationService.insertSysAuthorization(sysAuthorization));
    }

    /**
     * 修改授权信息
     */
    @PreAuthorize("@ss.hasPermi('tenant:authorization:edit')")
    @Log(title = "授权信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysAuthorization sysAuthorization)
    {
        return toAjax(sysAuthorizationService.updateSysAuthorization(sysAuthorization));
    }

    /**
     * 删除授权信息
     */
    @PreAuthorize("@ss.hasPermi('tenant:authorization:remove')")
    @Log(title = "授权信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(sysAuthorizationService.deleteSysAuthorizationByIds(ids));
    }
}
