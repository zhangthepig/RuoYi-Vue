package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysAuthorizationMenu;
import com.ruoyi.system.service.ISysAuthorizationMenuService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 授权菜单Controller
 * 
 * @author hy
 * @date 2021-01-06
 */
@RestController
@RequestMapping("/tenant/authorization/menu")
public class SysAuthorizationMenuController extends BaseController
{
    @Autowired
    private ISysAuthorizationMenuService sysAuthorizationMenuService;

    /**
     * 查询授权菜单列表
     */
    @PreAuthorize("@ss.hasPermi('authorization:menu:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysAuthorizationMenu sysAuthorizationMenu)
    {
        startPage();
        List<SysAuthorizationMenu> list = sysAuthorizationMenuService.selectSysAuthorizationMenuList(sysAuthorizationMenu);
        return getDataTable(list);
    }

    /**
     * 导出授权菜单列表
     */
    @PreAuthorize("@ss.hasPermi('authorization:menu:export')")
    @Log(title = "授权菜单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysAuthorizationMenu sysAuthorizationMenu)
    {
        List<SysAuthorizationMenu> list = sysAuthorizationMenuService.selectSysAuthorizationMenuList(sysAuthorizationMenu);
        ExcelUtil<SysAuthorizationMenu> util = new ExcelUtil<SysAuthorizationMenu>(SysAuthorizationMenu.class);
        return util.exportExcel(list, "menu");
    }

    /**
     * 获取授权菜单详细信息
     */
    @PreAuthorize("@ss.hasPermi('authorization:menu:query')")
    @GetMapping(value = "/{authorizationId}")
    public AjaxResult getInfo(@PathVariable("authorizationId") Long authorizationId)
    {
        return AjaxResult.success(sysAuthorizationMenuService.selectSysAuthorizationMenuById(authorizationId));
    }
}
