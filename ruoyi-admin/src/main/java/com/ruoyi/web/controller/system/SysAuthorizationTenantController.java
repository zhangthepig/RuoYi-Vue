package com.ruoyi.web.controller.system;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.SysAuthorizationTenant;
import com.ruoyi.system.service.ISysAuthorizationTenantService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 租户授权Controller
 * 
 * @author ruoyi
 * @date 2021-01-06
 */
@RestController
@RequestMapping("/system/authorization/tenant")
public class SysAuthorizationTenantController extends BaseController
{
    @Autowired
    private ISysAuthorizationTenantService sysAuthorizationTenantService;

    /**
     * 查询租户授权列表
     */
    @PreAuthorize("@ss.hasPermi('authorization:tenant:list')")
    @GetMapping("/list")
    public TableDataInfo list(SysAuthorizationTenant sysAuthorizationTenant)
    {
        startPage();
        List<SysAuthorizationTenant> list = sysAuthorizationTenantService.selectSysAuthorizationTenantList(sysAuthorizationTenant);
        return getDataTable(list);
    }

    /**
     * 导出租户授权列表
     */
    @PreAuthorize("@ss.hasPermi('authorization:tenant:export')")
    @Log(title = "租户授权", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SysAuthorizationTenant sysAuthorizationTenant)
    {
        List<SysAuthorizationTenant> list = sysAuthorizationTenantService.selectSysAuthorizationTenantList(sysAuthorizationTenant);
        ExcelUtil<SysAuthorizationTenant> util = new ExcelUtil<SysAuthorizationTenant>(SysAuthorizationTenant.class);
        return util.exportExcel(list, "tenant");
    }

    /**
     * 获取租户授权详细信息
     */
    @PreAuthorize("@ss.hasPermi('authorization:tenant:query')")
    @GetMapping(value = "/{authorizationId}")
    public AjaxResult getInfo(@PathVariable("authorizationId") Long authorizationId)
    {
        return AjaxResult.success(sysAuthorizationTenantService.selectSysAuthorizationTenantById(authorizationId));
    }

    /**
     * 新增租户授权
     */
    @PreAuthorize("@ss.hasPermi('authorization:tenant:add')")
    @Log(title = "租户授权", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SysAuthorizationTenant sysAuthorizationTenant)
    {
        return toAjax(sysAuthorizationTenantService.insertSysAuthorizationTenant(sysAuthorizationTenant));
    }

    /**
     * 修改租户授权
     */
    @PreAuthorize("@ss.hasPermi('authorization:tenant:edit')")
    @Log(title = "租户授权", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SysAuthorizationTenant sysAuthorizationTenant)
    {
        return toAjax(sysAuthorizationTenantService.updateSysAuthorizationTenant(sysAuthorizationTenant));
    }

    /**
     * 删除租户授权
     */
    @PreAuthorize("@ss.hasPermi('authorization:tenant:remove')")
    @Log(title = "租户授权", businessType = BusinessType.DELETE)
	@DeleteMapping("/{authorizationIds}")
    public AjaxResult remove(@PathVariable Long[] authorizationIds)
    {
        return toAjax(sysAuthorizationTenantService.deleteSysAuthorizationTenantByIds(authorizationIds));
    }
}
