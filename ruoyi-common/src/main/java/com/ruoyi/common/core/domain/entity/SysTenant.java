package com.ruoyi.common.core.domain.entity;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 租户管理对象 sys_tenant
 * 
 * @author hy
 * @date 2021-01-04
 */
public class SysTenant extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long tenantId;

    /** 租户名称 */
    @Excel(name = "租户名称")
    private String tenantName;

    /** 地址 */
    @Excel(name = "地址")
    private String address;

    /** 联系方式 */
    @Excel(name = "联系方式")
    private String phone;

    /** 营业执照号码 */
    @Excel(name = "营业执照号码")
    private String businessLicense;

    /** 法人姓名 */
    @Excel(name = "法人姓名")
    private String ownerName;

    /** 法人身份证号 */
    @Excel(name = "法人身份证号")
    private String ownerIdcard;

    /** 附件 */
    @Excel(name = "附件")
    private String enclosure;

    /** 数据库ID */
    @Excel(name = "数据库ID")
    private Long databaseId;

    public void setTenantId(Long tenantId) 
    {
        this.tenantId = tenantId;
    }

    public Long getTenantId() 
    {
        return tenantId;
    }
    public void setTenantName(String tenantName) 
    {
        this.tenantName = tenantName;
    }

    public String getTenantName() 
    {
        return tenantName;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setPhone(String phone) 
    {
        this.phone = phone;
    }

    public String getPhone() 
    {
        return phone;
    }
    public void setBusinessLicense(String businessLicense) 
    {
        this.businessLicense = businessLicense;
    }

    public String getBusinessLicense() 
    {
        return businessLicense;
    }
    public void setOwnerName(String ownerName) 
    {
        this.ownerName = ownerName;
    }

    public String getOwnerName() 
    {
        return ownerName;
    }
    public void setOwnerIdcard(String ownerIdcard) 
    {
        this.ownerIdcard = ownerIdcard;
    }

    public String getOwnerIdcard() 
    {
        return ownerIdcard;
    }
    public void setEnclosure(String enclosure) 
    {
        this.enclosure = enclosure;
    }

    public String getEnclosure() 
    {
        return enclosure;
    }
    public void setDatabaseId(Long databaseId) 
    {
        this.databaseId = databaseId;
    }

    public Long getDatabaseId() 
    {
        return databaseId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("tenantId", getTenantId())
            .append("tenantName", getTenantName())
            .append("address", getAddress())
            .append("phone", getPhone())
            .append("businessLicense", getBusinessLicense())
            .append("ownerName", getOwnerName())
            .append("ownerIdcard", getOwnerIdcard())
            .append("enclosure", getEnclosure())
            .append("databaseId", getDatabaseId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
