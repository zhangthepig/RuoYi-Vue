package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 授权菜单对象 sys_authorization_menu
 * 
 * @author hy
 * @date 2021-01-06
 */
public class SysAuthorizationMenu extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long authorizationId;

    /** $column.columnComment */
    @Excel(name = "${comment}", readConverterExp = "$column.readConverterExp()")
    private Long menuId;

    public void setAuthorizationId(Long authorizationId) 
    {
        this.authorizationId = authorizationId;
    }

    public Long getAuthorizationId() 
    {
        return authorizationId;
    }
    public void setMenuId(Long menuId) 
    {
        this.menuId = menuId;
    }

    public Long getMenuId() 
    {
        return menuId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("authorizationId", getAuthorizationId())
            .append("menuId", getMenuId())
            .toString();
    }
}
