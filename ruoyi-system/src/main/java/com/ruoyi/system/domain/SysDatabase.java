package com.ruoyi.system.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 数据源对象 sys_database
 * 
 * @author hy
 * @date 2021-01-04
 */
public class SysDatabase extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long databaseId;

    /** 链接名 */
    @Excel(name = "链接名")
    private String linkName;

    /** IP */
    @Excel(name = "IP")
    private String ip;

    /** 端口 */
    @Excel(name = "端口")
    private Integer port;

    /** 库名 */
    @Excel(name = "库名")
    private String databaseName;

    /** 用户名 */
    @Excel(name = "用户名")
    private String username;

    /** 密码 */
    @Excel(name = "密码")
    private String password;

    public void setDatabaseId(Long databaseId) 
    {
        this.databaseId = databaseId;
    }

    public Long getDatabaseId() 
    {
        return databaseId;
    }
    public void setLinkName(String linkName) 
    {
        this.linkName = linkName;
    }

    public String getLinkName() 
    {
        return linkName;
    }
    public void setIp(String ip) 
    {
        this.ip = ip;
    }

    public String getIp() 
    {
        return ip;
    }
    public void setPort(Integer port) 
    {
        this.port = port;
    }

    public Integer getPort() 
    {
        return port;
    }
    public void setDatabaseName(String databaseName) 
    {
        this.databaseName = databaseName;
    }

    public String getDatabaseName() 
    {
        return databaseName;
    }
    public void setUsername(String username) 
    {
        this.username = username;
    }

    public String getUsername() 
    {
        return username;
    }
    public void setPassword(String password) 
    {
        this.password = password;
    }

    public String getPassword() 
    {
        return password;
    }

    public String getUrl(){
        return "jdbc:mysql://"+ip+":"+port+"/"+databaseName+"?useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=convertToNull&useSSL=true&serverTimezone=GMT%2B8";
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("databaseId", getDatabaseId())
            .append("linkName", getLinkName())
            .append("ip", getIp())
            .append("port", getPort())
            .append("databaseName", getDatabaseName())
            .append("username", getUsername())
            .append("password", getPassword())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
