package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysDatabase;

/**
 * 数据源Mapper接口
 * 
 * @author hy
 * @date 2021-01-04
 */
public interface DatabaseMapper 
{
    /**
     * 查询数据源
     * 
     * @param databaseId 数据源ID
     * @return 数据源
     */
    public SysDatabase selectDatabaseById(Long databaseId);

    /**
     * 查询数据源列表
     * 
     * @param database 数据源
     * @return 数据源集合
     */
    public List<SysDatabase> selectDatabaseList(SysDatabase database);

    /**
     * 新增数据源
     * 
     * @param database 数据源
     * @return 结果
     */
    public int insertDatabase(SysDatabase database);

    /**
     * 修改数据源
     * 
     * @param database 数据源
     * @return 结果
     */
    public int updateDatabase(SysDatabase database);

    /**
     * 删除数据源
     * 
     * @param databaseId 数据源ID
     * @return 结果
     */
    public int deleteDatabaseById(Long databaseId);

    /**
     * 批量删除数据源
     * 
     * @param databaseIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteDatabaseByIds(Long[] databaseIds);
}
