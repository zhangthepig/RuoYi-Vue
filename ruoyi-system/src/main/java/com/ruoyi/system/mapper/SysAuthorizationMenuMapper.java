package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysAuthorizationMenu;

/**
 * 授权菜单Mapper接口
 * 
 * @author hy
 * @date 2021-01-06
 */
public interface SysAuthorizationMenuMapper 
{
    /**
     * 查询授权菜单
     * 
     * @param authorizationId 授权菜单ID
     * @return 授权菜单
     */
    public SysAuthorizationMenu selectSysAuthorizationMenuById(Long authorizationId);

    /**
     * 查询授权菜单列表
     * 
     * @param sysAuthorizationMenu 授权菜单
     * @return 授权菜单集合
     */
    public List<SysAuthorizationMenu> selectSysAuthorizationMenuList(SysAuthorizationMenu sysAuthorizationMenu);


    /**
     * 删除授权菜单
     * 
     * @param authorizationId 授权菜单ID
     * @return 结果
     */
    public int deleteSysAuthorizationMenuById(Long authorizationId);

    public int batchAuthorizationMenu(List<SysAuthorizationMenu> list);
}
