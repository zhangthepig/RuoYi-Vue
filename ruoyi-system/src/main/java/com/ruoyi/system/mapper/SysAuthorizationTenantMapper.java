package com.ruoyi.system.mapper;

import java.util.List;
import com.ruoyi.system.domain.SysAuthorizationTenant;

/**
 * 租户授权Mapper接口
 * 
 * @author ruoyi
 * @date 2021-01-06
 */
public interface SysAuthorizationTenantMapper 
{
    /**
     * 查询租户授权
     * 
     * @param authorizationId 租户授权ID
     * @return 租户授权
     */
    public SysAuthorizationTenant selectSysAuthorizationTenantById(Long authorizationId);

    /**
     * 查询租户授权列表
     * 
     * @param sysAuthorizationTenant 租户授权
     * @return 租户授权集合
     */
    public List<SysAuthorizationTenant> selectSysAuthorizationTenantList(SysAuthorizationTenant sysAuthorizationTenant);

    /**
     * 新增租户授权
     * 
     * @param sysAuthorizationTenant 租户授权
     * @return 结果
     */
    public int insertSysAuthorizationTenant(SysAuthorizationTenant sysAuthorizationTenant);

    /**
     * 修改租户授权
     * 
     * @param sysAuthorizationTenant 租户授权
     * @return 结果
     */
    public int updateSysAuthorizationTenant(SysAuthorizationTenant sysAuthorizationTenant);

    /**
     * 删除租户授权
     * 
     * @param authorizationId 租户授权ID
     * @return 结果
     */
    public int deleteSysAuthorizationTenantById(Long authorizationId);

    /**
     * 批量删除租户授权
     * 
     * @param authorizationIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteSysAuthorizationTenantByIds(Long[] authorizationIds);
}
