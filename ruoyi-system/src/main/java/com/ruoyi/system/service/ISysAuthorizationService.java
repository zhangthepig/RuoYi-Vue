package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.SysAuthorization;

/**
 * 授权信息Service接口
 * 
 * @author hy
 * @date 2021-01-06
 */
public interface ISysAuthorizationService 
{
    /**
     * 查询授权信息
     * 
     * @param id 授权信息ID
     * @return 授权信息
     */
    public SysAuthorization selectSysAuthorizationById(Long id);

    /**
     * 查询授权信息列表
     * 
     * @param sysAuthorization 授权信息
     * @return 授权信息集合
     */
    public List<SysAuthorization> selectSysAuthorizationList(SysAuthorization sysAuthorization);

    /**
     * 新增授权信息
     * 
     * @param sysAuthorization 授权信息
     * @return 结果
     */
    public int insertSysAuthorization(SysAuthorization sysAuthorization);

    /**
     * 修改授权信息
     * 
     * @param sysAuthorization 授权信息
     * @return 结果
     */
    public int updateSysAuthorization(SysAuthorization sysAuthorization);

    /**
     * 批量删除授权信息
     * 
     * @param ids 需要删除的授权信息ID
     * @return 结果
     */
    public int deleteSysAuthorizationByIds(Long[] ids);

    /**
     * 删除授权信息信息
     * 
     * @param id 授权信息ID
     * @return 结果
     */
    public int deleteSysAuthorizationById(Long id);
}
