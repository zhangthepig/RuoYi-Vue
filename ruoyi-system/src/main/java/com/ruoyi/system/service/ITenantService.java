package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.common.core.domain.entity.SysTenant;

/**
 * 租户管理Service接口
 * 
 * @author hy
 * @date 2021-01-04
 */
public interface ITenantService 
{
    /**
     * 查询租户管理
     * 
     * @param tenantId 租户管理ID
     * @return 租户管理
     */
    public SysTenant selectTenantById(Long tenantId);

    /**
     * 查询租户管理列表
     * 
     * @param tenant 租户管理
     * @return 租户管理集合
     */
    public List<SysTenant> selectTenantList(SysTenant tenant);

    /**
     * 新增租户管理
     * 
     * @param tenant 租户管理
     * @return 结果
     */
    public int insertTenant(SysTenant tenant);

    /**
     * 修改租户管理
     * 
     * @param tenant 租户管理
     * @return 结果
     */
    public int updateTenant(SysTenant tenant);

    /**
     * 批量删除租户管理
     * 
     * @param tenantIds 需要删除的租户管理ID
     * @return 结果
     */
    public int deleteTenantByIds(Long[] tenantIds);

    /**
     * 删除租户管理信息
     * 
     * @param tenantId 租户管理ID
     * @return 结果
     */
    public int deleteTenantById(Long tenantId);
}
