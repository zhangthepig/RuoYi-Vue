package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.DatabaseMapper;
import com.ruoyi.system.domain.SysDatabase;
import com.ruoyi.system.service.IDatabaseService;

/**
 * 数据源Service业务层处理
 * 
 * @author hy
 * @date 2021-01-04
 */
@Service
public class DatabaseServiceImpl implements IDatabaseService 
{
    @Autowired
    private DatabaseMapper databaseMapper;

    /**
     * 查询数据源
     * 
     * @param databaseId 数据源ID
     * @return 数据源
     */
    @Override
    public SysDatabase selectDatabaseById(Long databaseId)
    {
        return databaseMapper.selectDatabaseById(databaseId);
    }

    /**
     * 查询数据源列表
     * 
     * @param database 数据源
     * @return 数据源
     */
    @Override
    public List<SysDatabase> selectDatabaseList(SysDatabase database)
    {
        return databaseMapper.selectDatabaseList(database);
    }

    @Override
    public List<SysDatabase> selectDatabaseList()
    {
        return databaseMapper.selectDatabaseList(new SysDatabase());
    }

    /**
     * 新增数据源
     * 
     * @param database 数据源
     * @return 结果
     */
    @Override
    public int insertDatabase(SysDatabase database)
    {
        database.setCreateTime(DateUtils.getNowDate());
        return databaseMapper.insertDatabase(database);
    }

    /**
     * 修改数据源
     * 
     * @param database 数据源
     * @return 结果
     */
    @Override
    public int updateDatabase(SysDatabase database)
    {
        database.setUpdateTime(DateUtils.getNowDate());
        return databaseMapper.updateDatabase(database);
    }

    /**
     * 批量删除数据源
     * 
     * @param databaseIds 需要删除的数据源ID
     * @return 结果
     */
    @Override
    public int deleteDatabaseByIds(Long[] databaseIds)
    {
        return databaseMapper.deleteDatabaseByIds(databaseIds);
    }

    /**
     * 删除数据源信息
     * 
     * @param databaseId 数据源ID
     * @return 结果
     */
    @Override
    public int deleteDatabaseById(Long databaseId)
    {
        return databaseMapper.deleteDatabaseById(databaseId);
    }
}
