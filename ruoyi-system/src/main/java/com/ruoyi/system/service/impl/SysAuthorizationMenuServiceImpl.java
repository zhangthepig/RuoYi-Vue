package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysAuthorizationMenuMapper;
import com.ruoyi.system.domain.SysAuthorizationMenu;
import com.ruoyi.system.service.ISysAuthorizationMenuService;

/**
 * 授权菜单Service业务层处理
 * 
 * @author hy
 * @date 2021-01-06
 */
@Service
public class SysAuthorizationMenuServiceImpl implements ISysAuthorizationMenuService 
{
    @Autowired
    private SysAuthorizationMenuMapper sysAuthorizationMenuMapper;

    /**
     * 查询授权菜单
     * 
     * @param authorizationId 授权菜单ID
     * @return 授权菜单
     */
    @Override
    public SysAuthorizationMenu selectSysAuthorizationMenuById(Long authorizationId)
    {
        return sysAuthorizationMenuMapper.selectSysAuthorizationMenuById(authorizationId);
    }

    /**
     * 查询授权菜单列表
     * 
     * @param sysAuthorizationMenu 授权菜单
     * @return 授权菜单
     */
    @Override
    public List<SysAuthorizationMenu> selectSysAuthorizationMenuList(SysAuthorizationMenu sysAuthorizationMenu)
    {
        return sysAuthorizationMenuMapper.selectSysAuthorizationMenuList(sysAuthorizationMenu);
    }


    /**
     * 删除授权菜单信息
     * 
     * @param authorizationId 授权菜单ID
     * @return 结果
     */
    @Override
    public int deleteSysAuthorizationMenuById(Long authorizationId)
    {
        return sysAuthorizationMenuMapper.deleteSysAuthorizationMenuById(authorizationId);
    }
}
