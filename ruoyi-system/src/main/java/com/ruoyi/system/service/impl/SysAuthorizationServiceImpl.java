package com.ruoyi.system.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.core.domain.entity.SysRole;
import com.ruoyi.system.domain.SysAuthorizationMenu;
import com.ruoyi.system.domain.SysRoleMenu;
import com.ruoyi.system.mapper.SysAuthorizationMenuMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysAuthorizationMapper;
import com.ruoyi.system.domain.SysAuthorization;
import com.ruoyi.system.service.ISysAuthorizationService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 授权信息Service业务层处理
 * 
 * @author hy
 * @date 2021-01-06
 */
@Service
public class SysAuthorizationServiceImpl implements ISysAuthorizationService 
{
    @Autowired
    private SysAuthorizationMapper sysAuthorizationMapper;
    @Autowired
    private SysAuthorizationMenuMapper authorizationMenuMapper;

    /**
     * 查询授权信息
     * 
     * @param id 授权信息ID
     * @return 授权信息
     */
    @Override
    public SysAuthorization selectSysAuthorizationById(Long id)
    {
        return sysAuthorizationMapper.selectSysAuthorizationById(id);
    }

    /**
     * 查询授权信息列表
     * 
     * @param sysAuthorization 授权信息
     * @return 授权信息
     */
    @Override
    public List<SysAuthorization> selectSysAuthorizationList(SysAuthorization sysAuthorization)
    {
        return sysAuthorizationMapper.selectSysAuthorizationList(sysAuthorization);
    }

    /**
     * 新增授权信息
     * 
     * @param sysAuthorization 授权信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertSysAuthorization(SysAuthorization sysAuthorization)
    {
        sysAuthorizationMapper.insertSysAuthorization(sysAuthorization);
        return insertAuthorizationMenu(sysAuthorization);
    }

    /**
     * 修改授权信息
     * 
     * @param sysAuthorization 授权信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateSysAuthorization(SysAuthorization sysAuthorization)
    {
        sysAuthorizationMapper.updateSysAuthorization(sysAuthorization);
        authorizationMenuMapper.deleteSysAuthorizationMenuById(sysAuthorization.getId());
        return insertAuthorizationMenu(sysAuthorization);
    }


    public int insertAuthorizationMenu(SysAuthorization authorization)
    {
        int rows = 1;
        // 新增用户与角色管理
        List<SysAuthorizationMenu> list = new ArrayList<SysAuthorizationMenu>();
        for (Long menuId : authorization.getMenuIds())
        {
            SysAuthorizationMenu am = new SysAuthorizationMenu();
            am.setAuthorizationId(authorization.getId());
            am.setMenuId(menuId);
            list.add(am);
        }
        if (list.size() > 0)
        {
            rows = authorizationMenuMapper.batchAuthorizationMenu(list);
        }
        return rows;
    }
    /**
     * 批量删除授权信息
     * 
     * @param ids 需要删除的授权信息ID
     * @return 结果
     */
    @Override
    public int deleteSysAuthorizationByIds(Long[] ids)
    {
        return sysAuthorizationMapper.deleteSysAuthorizationByIds(ids);
    }

    /**
     * 删除授权信息信息
     * 
     * @param id 授权信息ID
     * @return 结果
     */
    @Override
    public int deleteSysAuthorizationById(Long id)
    {
        return sysAuthorizationMapper.deleteSysAuthorizationById(id);
    }
}
