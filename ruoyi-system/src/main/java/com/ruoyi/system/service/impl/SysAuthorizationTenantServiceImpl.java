package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.SysAuthorizationTenantMapper;
import com.ruoyi.system.domain.SysAuthorizationTenant;
import com.ruoyi.system.service.ISysAuthorizationTenantService;

/**
 * 租户授权Service业务层处理
 * 
 * @author ruoyi
 * @date 2021-01-06
 */
@Service
public class SysAuthorizationTenantServiceImpl implements ISysAuthorizationTenantService 
{
    @Autowired
    private SysAuthorizationTenantMapper sysAuthorizationTenantMapper;

    /**
     * 查询租户授权
     * 
     * @param authorizationId 租户授权ID
     * @return 租户授权
     */
    @Override
    public SysAuthorizationTenant selectSysAuthorizationTenantById(Long authorizationId)
    {
        return sysAuthorizationTenantMapper.selectSysAuthorizationTenantById(authorizationId);
    }

    /**
     * 查询租户授权列表
     * 
     * @param sysAuthorizationTenant 租户授权
     * @return 租户授权
     */
    @Override
    public List<SysAuthorizationTenant> selectSysAuthorizationTenantList(SysAuthorizationTenant sysAuthorizationTenant)
    {
        return sysAuthorizationTenantMapper.selectSysAuthorizationTenantList(sysAuthorizationTenant);
    }

    /**
     * 新增租户授权
     * 
     * @param sysAuthorizationTenant 租户授权
     * @return 结果
     */
    @Override
    public int insertSysAuthorizationTenant(SysAuthorizationTenant sysAuthorizationTenant)
    {
        return sysAuthorizationTenantMapper.insertSysAuthorizationTenant(sysAuthorizationTenant);
    }

    /**
     * 修改租户授权
     * 
     * @param sysAuthorizationTenant 租户授权
     * @return 结果
     */
    @Override
    public int updateSysAuthorizationTenant(SysAuthorizationTenant sysAuthorizationTenant)
    {
        return sysAuthorizationTenantMapper.updateSysAuthorizationTenant(sysAuthorizationTenant);
    }

    /**
     * 批量删除租户授权
     * 
     * @param authorizationIds 需要删除的租户授权ID
     * @return 结果
     */
    @Override
    public int deleteSysAuthorizationTenantByIds(Long[] authorizationIds)
    {
        return sysAuthorizationTenantMapper.deleteSysAuthorizationTenantByIds(authorizationIds);
    }

    /**
     * 删除租户授权信息
     * 
     * @param authorizationId 租户授权ID
     * @return 结果
     */
    @Override
    public int deleteSysAuthorizationTenantById(Long authorizationId)
    {
        return sysAuthorizationTenantMapper.deleteSysAuthorizationTenantById(authorizationId);
    }
}
