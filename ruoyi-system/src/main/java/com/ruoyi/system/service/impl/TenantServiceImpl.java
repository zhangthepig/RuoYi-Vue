package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.TenantMapper;
import com.ruoyi.common.core.domain.entity.SysTenant;
import com.ruoyi.system.service.ITenantService;

/**
 * 租户管理Service业务层处理
 * 
 * @author hy
 * @date 2021-01-04
 */
@Service
public class TenantServiceImpl implements ITenantService 
{
    @Autowired
    private TenantMapper tenantMapper;

    /**
     * 查询租户管理
     * 
     * @param tenantId 租户管理ID
     * @return 租户管理
     */
    @Override
    public SysTenant selectTenantById(Long tenantId)
    {
        return tenantMapper.selectTenantById(tenantId);
    }

    /**
     * 查询租户管理列表
     * 
     * @param tenant 租户管理
     * @return 租户管理
     */
    @Override
    public List<SysTenant> selectTenantList(SysTenant tenant)
    {
        return tenantMapper.selectTenantList(tenant);
    }

    /**
     * 新增租户管理
     * 
     * @param tenant 租户管理
     * @return 结果
     */
    @Override
    public int insertTenant(SysTenant tenant)
    {
        tenant.setCreateTime(DateUtils.getNowDate());
        return tenantMapper.insertTenant(tenant);
    }

    /**
     * 修改租户管理
     * 
     * @param tenant 租户管理
     * @return 结果
     */
    @Override
    public int updateTenant(SysTenant tenant)
    {
        tenant.setUpdateTime(DateUtils.getNowDate());
        return tenantMapper.updateTenant(tenant);
    }

    /**
     * 批量删除租户管理
     * 
     * @param tenantIds 需要删除的租户管理ID
     * @return 结果
     */
    @Override
    public int deleteTenantByIds(Long[] tenantIds)
    {
        return tenantMapper.deleteTenantByIds(tenantIds);
    }

    /**
     * 删除租户管理信息
     * 
     * @param tenantId 租户管理ID
     * @return 结果
     */
    @Override
    public int deleteTenantById(Long tenantId)
    {
        return tenantMapper.deleteTenantById(tenantId);
    }
}
