import request from '@/utils/request'

// 查询授权信息列表
export function listAuthorization(query) {
  return request({
    url: '/tenant/authorization/list',
    method: 'get',
    params: query
  })
}

// 查询授权信息详细
export function getAuthorization(id) {
  return request({
    url: '/tenant/authorization/' + id,
    method: 'get'
  })
}

// 新增授权信息
export function addAuthorization(data) {
  return request({
    url: '/tenant/authorization',
    method: 'post',
    data: data
  })
}

// 修改授权信息
export function updateAuthorization(data) {
  return request({
    url: '/tenant/authorization',
    method: 'put',
    data: data
  })
}

// 删除授权信息
export function delAuthorization(id) {
  return request({
    url: '/tenant/authorization/' + id,
    method: 'delete'
  })
}

// 导出授权信息
export function exportAuthorization(query) {
  return request({
    url: '/tenant/authorization/export',
    method: 'get',
    params: query
  })
}
