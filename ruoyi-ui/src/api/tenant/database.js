import request from '@/utils/request'

// 查询数据源列表
export function listDatabase(query) {
  return request({
    url: '/tenant/database/list',
    method: 'get',
    params: query
  })
}
// 查询数据源列表
export function databaseList(query) {
  return request({
    url: '/tenant/database/listWithOutPage?databaseId=0',
    method: 'get'//,
    //params: {databaseId:0}//query
  })
}

// 查询数据源详细
export function getDatabase(databaseId) {
  return request({
    url: '/tenant/database/' + databaseId,
    method: 'get'
  })
}

// 新增数据源
export function addDatabase(data) {
  return request({
    url: '/tenant/database',
    method: 'post',
    data: data
  })
}

// 修改数据源
export function updateDatabase(data) {
  return request({
    url: '/tenant/database',
    method: 'put',
    data: data
  })
}

// 删除数据源
export function delDatabase(databaseId) {
  return request({
    url: '/tenant/database/' + databaseId,
    method: 'delete'
  })
}
